package br.com.itau.consultacep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ConsultaCepZipklinApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultaCepZipklinApplication.class, args);
	}

}
