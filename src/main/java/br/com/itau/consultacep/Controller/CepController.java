package br.com.itau.consultacep.Controller;
import br.com.itau.consultacep.client.CepClient;

import br.com.itau.consultacep.client.Cep;
import br.com.itau.consultacep.client.CepClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CepController {

@Autowired
private CepClient cepClient;


    @GetMapping("/{cep}")
    public Cep consultaCep(@PathVariable String cep) {

        return  cepClient.buscarCep(cep);

    }

}
